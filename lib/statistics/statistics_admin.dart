import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monitoring/widget/Custom_AppBar.dart';
import 'package:monitoring/widget/colors.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class Department {
  String name;
  int visitorCount;
  DateTime time;

  Department(this.name, this.visitorCount, this.time);
}

List<Department> departments = [
  Department('Building', 65, DateTime.now()),
  Department('Floor 1', 20, DateTime.now()),
  Department('Floor 2', 15, DateTime.now()),
  Department('Floor 3', 5, DateTime.now()),
  Department('Floor 4', 5, DateTime.now()),
  Department('Floor 5', 10, DateTime.now()),
  Department('Floor 6', 10, DateTime.now()),
];
List<ChartData> chartData = [
  // ChartData('Europe', 462),
  // ChartData('Africa', 257),
  // ChartData('Oceania', 43),
  // ChartData('Americas', 351),
];

class ChartData {
  ChartData(this.x, this.y);
  final String x;
  final double y;
}

class Statistics extends StatefulWidget {
  const Statistics({super.key});

  @override
  State<Statistics> createState() => _StatisticsState();
}

class _StatisticsState extends State<Statistics> {
  @override
  Widget build(BuildContext context) {
    for (int i = 1; i <= 23; i++) {
      chartData.add(ChartData('${i}:00', 504 * i.toDouble()));
    }
    return Scaffold(
      appBar: AppBarCustom(
        title: 'Statistics',
      ),
      body: Column(
        children: [
          SizedBox(
            height: 0.h,
            // child: SfCartesianChart(
            // primaryXAxis: CategoryAxis(),
            // primaryYAxis: NumericAxis(),
            // series: <ChartSeries>[
            // BarSeries<ChartData, String>(
            // dataSource: chartData,
            // xValueMapper: (ChartData data, _) => data.x,
            // yValueMapper: (ChartData data, _) => data.y,
            // color: AppColors.blueColor,
            // spacing: 0.1,
            // ),
            // ],
            // ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Text(
                  " List of floors",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.h,
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: departments.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: AppColors.blueColor.withOpacity(0.1),
                    ),
                    child: ListTile(
                      title: Text(
                        departments[index].name,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(
                        Icons.apartment,
                        color: AppColors.blueColor,
                      ),
                      onTap: () {
                        _showVisitorCount(departments[index]);
                      },
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showVisitorCount(Department department) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(department.name),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 2.w),
                child: Text(
                  'Employee Count:6\n Visitor Count: ${department.visitorCount}',
                  style: TextStyle(fontSize: 14.sp),
                ),
              ),
              Text(
                " time: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                style: TextStyle(fontSize: 14.sp),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Close'),
            ),
          ],
        );
      },
    );
  }
}
