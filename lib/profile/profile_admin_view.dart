import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:lottie/lottie.dart';
import 'package:monitoring/chat/home.dart';
import 'package:monitoring/login_view/login_view.dart';
import 'package:monitoring/notification/notification_admin.dart';
import 'package:monitoring/notification/assign_notification.dart';
import 'package:monitoring/statistics/statistics_admin.dart';
import 'package:monitoring/tasks/listOfTasks.dart';
import 'package:monitoring/widget/colors.dart';
import 'package:monitoring/widget/custom_button.dart';

class ProfileAdminView extends StatelessWidget {
  const ProfileAdminView({super.key});

  @override
  Widget build(BuildContext context) {
    ImageProvider profilePic;
    if (true) {
      profilePic = true
          ? const AssetImage('assets/profile-icon-female.png')
          : const AssetImage('assets/profile-icon-male1.png');
    }
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    color: Color.fromRGBO(88, 95, 122, 0.914),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: GestureDetector(
                              child: Container(
                            height: 120.h,
                            width: 120.w,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: AppColors.Grey2,
                                width: 2,
                              ),
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/profile-icon-malee.jpg'),
                                  fit: BoxFit.contain),
                            ),
                          )),
                        ),
                        SizedBox(
                          height: 40.h,
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 5.h),
                          child: Text(
                            "Adam Hana",
                            style: TextStyle(
                                fontSize: 27.sp,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 243, 245, 247)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 70.h),
                          child: Text("adam1@gmail.com",
                              style: TextStyle(
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromARGB(255, 172, 183, 193))
                              // color: AppColors.brown.withOpacity(0.3)),
                              ),
                        ),
                      ],
                    ),
                  )),
              Expanded(child: Container(color: Colors.white)),
            ],
          ),
          Align(
            alignment: Alignment(0, 5),
            child: Container(
              width: 600,
              height: 730,
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(40),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width:
                                  40, // Adjust the size of the container as per your requirements
                              height: 40,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromRGBO(1, 7, 30,
                                      0.916) // Customize the color of the container as needed
                                  ),
                              child: Icon(
                                Icons.notifications,
                                color: Colors
                                    .white, // Customize the color of the icon as needed
                              ),
                            ),

                            // Adjust the spacing between the icon and text
                            Padding(
                              padding: const EdgeInsets.only(right: 85),
                              child: Text(
                                ' Notifications',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.brown),
                              ),
                            ),

                            InkWell(
                              onTap: () {
                                Get.to(NotificationListView());
                              },
                              child: Icon(
                                Icons.arrow_circle_right,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                size:
                                    30, // Customize the color of the icon as needed
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width:
                                  40, // Adjust the size of the container as per your requirements
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                // Customize the color of the container as needed
                              ),
                              child: Icon(
                                Icons.notification_add,
                                color: Colors
                                    .white, // Customize the color of the icon as needed
                              ),
                            ),

                            // Adjust the spacing between the icon and text
                            Padding(
                              padding: const EdgeInsets.only(right: 2),
                              child: Text(
                                'Assignment of a task',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.brown),
                              ),
                            ),

                            InkWell(
                              onTap: () {
                                Get.to(AssignNotification());
                              },
                              child: Icon(
                                Icons.arrow_circle_right,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                size:
                                    30, // Customize the color of the icon as needed
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width:
                                  40, // Adjust the size of the container as per your requirements
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                // Customize the color of the container as needed
                              ),
                              child: Icon(
                                Icons.task,
                                color: Colors
                                    .white, // Customize the color of the icon as needed
                              ),
                            ),

                            // Adjust the spacing between the icon and text
                            Padding(
                              padding: EdgeInsets.only(right: 63.w),
                              child: Text(
                                'List of a tasks',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.brown),
                              ),
                            ),

                            InkWell(
                              onTap: () {
                                Get.to(TaskList());
                              },
                              child: Icon(
                                Icons.arrow_circle_right,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                size:
                                    30, // Customize the color of the icon as needed
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width:
                                  40, // Adjust the size of the container as per your requirements
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(1, 7, 30,
                                    0.916), // Customize the color of the container as needed
                              ),
                              child: Icon(
                                Icons.chat,
                                color: Colors
                                    .white, // Customize the color of the icon as needed
                              ),
                            ),
                            // Adjust the spacing between the icon and text
                            Padding(
                              padding: const EdgeInsets.only(right: 155),
                              child: Text(
                                'Chat',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.brown),
                              ),
                            ),

                            InkWell(
                              onTap: () {
                                Get.to(Home());
                              },
                              child: Icon(
                                Icons.arrow_circle_right,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                size:
                                    30, // Customize the color of the icon as needed
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width:
                                  40, // Adjust the size of the container as per your requirements
                              height: 40,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromRGBO(1, 7, 30,
                                      0.916) // Customize the color of the container as needed
                                  ),
                              child: Icon(
                                Icons.show_chart_outlined,
                                color: Colors
                                    .white, // Customize the color of the icon as needed
                              ),
                            ),
                            // Adjust the spacing between the icon and text
                            Padding(
                              padding: const EdgeInsets.only(right: 120),
                              child: Text(
                                'Statistics',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.brown),
                              ),
                            ),

                            InkWell(
                              onTap: () {
                                Get.to(Statistics());
                              },
                              child: Icon(
                                Icons.arrow_circle_right,
                                color: Color.fromRGBO(1, 7, 30, 0.916),
                                size:
                                    30, // Customize the color of the icon as needed
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 50.h),
                        InkWell(
                          onTap: () {
                            Get.to(LoginAndroidView());
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                width:
                                    40, // Adjust the size of the container as per your requirements
                                height: 40,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromRGBO(1, 7, 30,
                                      0.916), // Customize the color of the container as needed
                                ),
                                child: Icon(
                                  Icons.logout,
                                  color: Colors
                                      .white, // Customize the color of the icon as needed
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              // Adjust the spacing between the icon and text
                              Text(
                                'Log out',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromARGB(255, 90, 58, 30)),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildImage(String assetName, [double width = 400]) {
  return Image.asset(
    'assets/$assetName',
    width: width,
    height: 500,
    fit: BoxFit.contain,
  );
}
