import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:lottie/lottie.dart';
import 'package:monitoring/chat/chat.dart';
import 'package:monitoring/notification/notification_employee.dart';
import 'package:monitoring/profile/profile_admin_view.dart';
import 'package:monitoring/widget/colors.dart';

// import 'package:monitoring/widget/custom_textform_field.dart';
class ProfileEmpolyeeView extends StatelessWidget {
  // final TextEditingController _working_hours_toController =
  //     TextEditingController();
  // final TextEditingController _working_hours_fromController =
  //     TextEditingController();
  // final TextEditingController _punishmentsController = TextEditingController();
  // final TextEditingController _rewardsController = TextEditingController();
  // final TextEditingController _salaryController = TextEditingController();
  // final TextEditingController _imageController = TextEditingController();
  // final TextEditingController _ageController = TextEditingController();
  // final TextEditingController _nameController = TextEditingController();
  // final TextEditingController _userController = TextEditingController();

  Future<dynamic> update(
      String name,
      String user,
      int salary,
      int working_hours_from,
      int working_hours_to,
      int age,
      Image image,
      String rewards,
      String punishments) async {
    try {
      // String name = _nameController.text;

      // String user = _userController.text;
      // int salary = int.parse(_salaryController.text);
      // int working_hours_from = int.parse(_working_hours_fromController.text);
      // int age = int.parse(_ageController.text);
      // String image = _imageController.text;
      // String rewards = _rewardsController.text;
      // String punishments = _punishmentsController.text;
      // int working_hours_to = int.parse(_working_hours_toController.text);
      // print("kjhgfdfghjkl;");
      // print(name);
      http.Response response = await http.get(
        Uri.parse('http://10.0.2.2:8000/ai/employees/'),
        // body: {
        //   'name': name,
        //   'user': user,
        //   'salary': salary,
        //   'working_hours_from': working_hours_from,
        //   'age': age,
        //   'image': image,
        //   'rewards': rewards,
        //   'punishments': punishments,
        //   'working_hours_to': working_hours_to,
        // },
      );
      // استخدم البيانات المسترجعة من الاستجابة هنا حسب الحاجة
    } catch (e) {
      print(e.toString());
    }
  }

  //bool? remember = false;
  final List<String?> errors = [];

  void updateProfile() {
    // Implement the logic to update the user's profile here
  }

  @override
  Widget build(BuildContext context) {
    ImageProvider profilePic;
    if (true) {
      profilePic = true
          ? const AssetImage('assets/profile-icon-female.png')
          : const AssetImage('assets/profile-icon-male1.png');
    }
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    color: AppColors.Grey1.withOpacity(0.5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: GestureDetector(
                              child: Container(
                            height: 120.h,
                            width: 120.w,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: AppColors.Grey2,
                                width: 2,
                              ),
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/profile-icon-female1.jpg'),
                                  fit: BoxFit.contain),
                            ),
                          )),
                        ),
                        SizedBox(
                          height: 15.h,
                        ),
                        Container(
                          child: Text(
                            "Sara Adnan",
                            // _nameController.text,
                            style: TextStyle(
                              fontSize: 20.sp,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 4, 77, 137),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 50.h),
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "work in The Department  ",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.mainWhiteColor)),
                              TextSpan(
                                  text: "Sales",
                                  style: TextStyle(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.mainBlackColor))
                            ]),
                          ),
                        )
                      ],
                    ),
                  )),
              Expanded(child: Container(color: Colors.white)),
            ],
          ),
          Align(
            alignment: Alignment(0, 1.7),
            child: Container(
              width: 600,
              height: 600,
              child: Card(
                child: ListView(children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Get.to(NotificationEmployee());
                                  },
                                  child: Container(
                                    width: 110,
                                    height: 130,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 6),
                                    decoration: BoxDecoration(
                                      color:
                                          AppColors.blueColor.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.3),
                                          offset: const Offset(0, 2),
                                          blurRadius: 5,
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 12.0),
                                          child: Center(
                                              child: Container(
                                            child: Lottie.asset("assets/1.json",
                                                animate: false),
                                          )),
                                        ),
                                        const SizedBox(height: 10),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 2.0),
                                            child: Text(
                                              "Notifcation",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Get.to(ChatScreen());
                                  },
                                  child: Container(
                                    width: 110,
                                    height: 130,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 6),
                                    decoration: BoxDecoration(
                                      color:
                                          AppColors.blueColor.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.3),
                                          offset: const Offset(0, 2),
                                          blurRadius: 5,
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 12.0, top: 10),
                                          child: Container(
                                              child: Image.asset(
                                            "assets/chat.png",
                                            width: 90,
                                            height: 70,
                                          )),
                                        ),
                                        const SizedBox(height: 10),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 30.0, top: 2),
                                          child: Text(
                                            "Chat",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black87,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 110,
                                  height: 130,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 6),
                                  decoration: BoxDecoration(
                                    color: AppColors.blueColor.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.3),
                                        offset: const Offset(0, 2),
                                        blurRadius: 5,
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: Container(
                                          child: Lottie.asset("assets/2.json",
                                              animate: false),
                                        ),
                                      ),
                                      const SizedBox(height: 10),
                                      InkWell(
                                        onTap: () {
                                          Get.to(ProfileAdminView());
                                        },
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Text(
                                            "Log Out",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black87,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "age",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: AppColors.mainBlackColor),
                                    ),
                                    Text(
                                      "23",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.blueColor),
                                    )
                                  ],
                                ),
                                Divider(
                                  thickness: 1.2,
                                  color: AppColors.Grey,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "gender",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: AppColors.mainBlackColor),
                                    ),
                                    Text(
                                      "Female",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.blueColor),
                                    )
                                  ],
                                ),
                                Divider(
                                  thickness: 1.2,
                                  color: AppColors.Grey,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Salary",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: AppColors.mainBlackColor),
                                    ),
                                    Text(
                                      "1000KW",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.blueColor),
                                    )
                                  ],
                                ),
                                Divider(
                                  thickness: 1.2,
                                  color: AppColors.Grey,
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Text(
                                "Work Hours:",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.mainBlackColor),
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            Container(
                              padding: EdgeInsets.all(5),
                              margin: EdgeInsets.all(2),
                              // decoration: BoxDecoration(
                              //     border: Border.all(
                              //         color: Colors.red, width: 2 // Border color
                              //         // Border width
                              //         ),
                              //     borderRadius: BorderRadius.circular(10.0)),
                              // margin: EdgeInsets.all(4),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.punch_clock,
                                        color: AppColors.blueColor,
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Text(
                                        "From",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                AppColors.secondaryBlackColor),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 12.w,
                                  ),
                                  Text(
                                    "8Am",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.secondaryBlackColor),
                                  ),
                                  SizedBox(width: 40.w),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Icon(
                                        Icons.punch_clock,
                                        color: AppColors.blueColor,
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Text(
                                        "To",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                AppColors.secondaryBlackColor),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 12.w,
                                  ),
                                  Text(
                                    "4pm",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.secondaryBlackColor),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Text(
                                "Rewards:",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.mainBlackColor),
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              itemCount: 1,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color.fromARGB(255, 71, 60, 33),
                                        width: 2 // Border color
                                        // Border width
                                        ),
                                    borderRadius: BorderRadius.circular(10.0),

                                    // Circular shape
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        "Salary increase",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                AppColors.secondaryBlackColor),
                                      ),
                                      SizedBox(
                                        width: 150.w,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: Colors.yellow,
                                      )
                                    ],
                                  ),
                                );
                              },
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Text(
                                "Punishments:",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.mainBlackColor),
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 60),
                              child: ListView.builder(
                                primary: true,
                                shrinkWrap: true,
                                itemCount: 1,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    padding: EdgeInsets.all(10),
                                    margin: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.red,
                                            width: 2 // Border color
                                            // Border width
                                            ),
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    // margin: EdgeInsets.all(4),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          "late of work",
                                          style: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors
                                                  .secondaryBlackColor),
                                        ),
                                        SizedBox(
                                          width: 150.w,
                                        ),
                                        Icon(
                                          Icons.warning,
                                          color: Colors.red,
                                        )
                                      ],
                                    ),
                                  );
                                },
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildImage(String assetName, [double width = 400]) {
  return Image.asset(
    'assets/$assetName',
    width: width,
    height: 500,
    fit: BoxFit.contain,
  );
}
