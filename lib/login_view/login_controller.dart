import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;

Future<dynamic> sendUserData(String email, String password) async {
  var url = Uri.http('10.0.2.2:8000/ai/login/');
  // Replace with your backend server URL
  print(password);
  http.Response response = await http.Client().post(url,
      body: jsonEncode({
        'email': email,
        'password': password,
      }));

  if (response.statusCode == 200) {
    print('Data sent successfully');
  } else {
    print('Failed to send data. Error: ${response.statusCode}');
  }
}

// Usage:///
//sendUserData('example@example.com', 'password123');