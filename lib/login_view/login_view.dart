import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:monitoring/login_view/login_controller.dart';
import 'package:monitoring/notification/admin_controller.dart';
import 'package:monitoring/profile/profile_admin_view.dart';
import 'package:monitoring/profile/profile_employee_view.dart';
import 'package:monitoring/widget/colors.dart';
import 'package:monitoring/widget/custom_button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:monitoring/widget/textForm.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginAndroidView extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool m = false;
  String n = 'h';
  bool n1 = false;
  Future<dynamic> sendUserData(String email, String password) async {
    var loginUrl = Uri.parse('http://10.0.2.2:8000/ai/login/');

    // var body = jsonEncode({"email": email, "password": "123"});
    print(email);
    http.Response response = await http.Client()
        .post(loginUrl, body: ({"email": email, "password": password}));
    if (response.statusCode == 200) {
      var responseData = jsonDecode(response.body);
      n = responseData['id'].toString();
      n1 = responseData['is_admin'];
      print(n1);
      m = true;
      print('Data sent successfully');
    } else {
      print('Failed to send data. Error: ${response.statusCode}');
    }
  }

  Future<void> savepre() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("id", n);
    print('save succes');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blueColor.withOpacity(0.3),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 58),
              Text(
                "Sign up",
                style: TextStyle(
                  fontSize: 36,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ClipOval(
                    child: Image.asset(
                      'assets/aaa.jpg',
                      width: 200,
                      height: 200,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 60.h),
              TextFormField(
                controller: _emailController,
                decoration: InputDecoration(
                  hintText: 'Email',
                  prefixIcon: Icon(Icons.person, color: Colors.white),
                  filled: true,
                  fillColor: Colors.white.withOpacity(0.3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide.none,
                  ),
                ),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 30.h),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock, color: Colors.white),
                  filled: true,
                  fillColor: Colors.white.withOpacity(0.3),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide.none,
                  ),
                ),
                obscureText: true,
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 35.h),
              CustomButton(
                text: 'Sign in',
                iconName: Icons.login,
                onPressed: () {
                  String email = _emailController.text;
                  String password = _passwordController.text;
                  print(_passwordController.text);
                  sendUserData(email, password);
                  if (m) {
                    if (n1) {
                      Get.to(ProfileEmpolyeeView());
                    } else {
                      Get.to(ProfileEmpolyeeView());
                    }
                  }
                },
                backgroundColor: AppColors.Grey1,
                textSize: 18.sp,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
