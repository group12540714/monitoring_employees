import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:monitoring/widget/Custom_AppBar.dart';
import 'package:monitoring/widget/colors.dart';

class Task {
  final String name;
  final String employee;
  final TaskState state;
  final DateTime start;
  final DateTime done;

  const Task({
    required this.name,
    required this.employee,
    required this.state,
    required this.start,
    required this.done,
  });
}

enum TaskState { newTask, inProgress, completed }

class TaskList extends StatefulWidget {
  const TaskList({Key? key}) : super(key: key);

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  List<Task> tasks = [
    Task(
      name: 'Task no status',
      employee: 'Ali Ahmad',
      state: TaskState.inProgress,
      start: DateTime(00, 00),
      done: DateTime(00, 00), // تعيين تاريخ ووقت انتهاء المهمة
    ),
    Task(
      name: 'Task is Active',
      employee: 'Alice Walker',
      state: TaskState.inProgress,
      start: DateTime(2024, 2, 3, 9, 1),
      done: DateTime(00, 00),
    ),
    Task(
      name: 'Task no status',
      employee: 'Assad aid',
      state: TaskState.inProgress,
      start: DateTime(00, 00),
      done: DateTime(00, 00),
    ),
    Task(
      name: 'Task is Active',
      employee: 'Tom Jones',
      state: TaskState.inProgress,
      start: DateTime(2024, 2, 3, 8, 12),
      done: DateTime(00, 00),
    ),
    Task(
      name: 'Task Finished',
      employee: 'Sara Adnan',
      state: TaskState.completed,
      start: DateTime(2024, 2, 3, 10, 12),
      done: DateTime(2024, 2, 3, 10, 33),
    ),
    Task(
      name: 'Task Finished',
      employee: 'Tom Jones',
      state: TaskState.completed,
      start: DateTime(2024, 2, 3, 1, 32),
      done: DateTime(2024, 2, 3, 1, 50),
    ),
    Task(
      name: 'Task Finished',
      employee: 'Alice Walker',
      state: TaskState.completed,
      start: DateTime(2024, 2, 3, 4, 12),
      done: DateTime(2024, 2, 3, 4, 30),
    ),
  ];
  List<Task> filterTasks = [];

  // Filter tasks based on state
  List<Task> filteredTasks(TaskState state) {
    return tasks.where((task) => task.state == state).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(title: "Lists of Tasks"),
      body: ListView.builder(
        itemCount: tasks.length,
        itemBuilder: (context, index) {
          final task = tasks[index];
          return TaskCard(task: task);
        },
      ),
    );
  }
}

class TaskCard extends StatelessWidget {
  final Task task;

  const TaskCard({required this.task});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Color.fromARGB(255, 235, 240, 247),
          // boxShadow: [
          //   BoxShadow(
          //     color: Colors.grey.withOpacity(0.5),
          //     blurRadius: 5.0,
          //     offset: Offset(0.0, 1.0), // Offset only vertically
          //     spreadRadius: -5.0, // Clip the shadow to the bottom
          //   ),
          // ],
        ),
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
            children: [
              CircleAvatar(
                child: Text(task.employee[0]),
                backgroundColor: AppColors.blueColor.withOpacity(0.7),
                foregroundColor: AppColors.mainWhiteColor,
              ),
              SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                        children: [
                          TextSpan(
                              text: task.name,
                              style:
                                  TextStyle(color: AppColors.mainBlackColor)),
                          TextSpan(
                              text: ' by ',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: AppColors.blueColor)),
                          TextSpan(
                              text: task.employee,
                              style:
                                  TextStyle(color: AppColors.mainBlackColor)),
                        ],
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Text(
                      _getTaskStateText(task.state),
                      style: TextStyle(color: _getTaskStateColor(task.state)),
                    ),
                    SizedBox(height: 4.0),
                    Text('Start: ${DateFormat('HH:mm').format(task.start)}'),
                    Text('Done: ${DateFormat('HH:mm').format(task.done)}'),
                  ],
                ),
              ),
              Icon(_getTaskStateIcon(task.state),
                  color: _getTaskStateColor(task.state)),
            ],
          ),
        ),
      ),
    );
  }

  String _getTaskStateText(TaskState state) {
    switch (state) {
      case TaskState.newTask:
        return 'Not started';
      case TaskState.inProgress:
        return 'In progress';
      case TaskState.completed:
        return 'Completed';
    }
  }

  Color _getTaskStateColor(TaskState state) {
    switch (state) {
      case TaskState.newTask:
        return Colors.grey;
      case TaskState.inProgress:
        return Colors.red;
      case TaskState.completed:
        return Colors.green;
    }
  }

  IconData _getTaskStateIcon(TaskState state) {
    switch (state) {
      case TaskState.newTask:
        return Icons.h_mobiledata;
      case TaskState.inProgress:
// Continuing from the previous code snippet:

        return Icons.hourglass_bottom;
      case TaskState.completed:
        return Icons.done;
      // TODO: Handle this case.
    }
  }
}
