import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:monitoring/widget/colors.dart';


// ignore: must_be_immutable
class AppBarCustom extends StatelessWidget implements PreferredSizeWidget {
  AppBarCustom({
    super.key,
    required this.title,
  });
  final String title;
 
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(child: Text(title)),
      leading: IconButton(
        icon: Icon(Icons.menu, size: 25.w,color:AppColors.blueColor ,),
        onPressed: () {
          
        },
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 10.w),
          child: IconButton(
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 20.w,
              color:AppColors.blueColor 
            ),
            onPressed: () {
              Get.back();
              // Handle search button tap
            },
          ),
        ),
      ],
    );
  }

  @override
  @override
  Size get preferredSize => Size(Get.width, 75.h);
}
