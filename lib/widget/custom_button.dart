import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:monitoring/widget/colors.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Color? textColor;
  final double? textSize;
  final FontWeight? textFontWeight;
  final Color? borderColor;
  final Color? backgroundColor;
  final Function onPressed;
  final String? svgName;
  final Size? fixedSize;
  final IconData? iconName;
  final BorderRadiusGeometry? borderRadius;
  const CustomButton({
    Key? key,
    required this.text,
    this.iconName,
    this.textColor,
    this.textSize,
    this.textFontWeight,
    this.borderColor,
    this.backgroundColor,
    required this.onPressed,
    this.svgName,
    this.fixedSize,
    this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressed();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        
          Text(
            text,
            style: TextStyle(
                color: textColor ?? AppColors.secondaryWhiteColor,
                fontSize: textSize ?? 18.sp,
                fontWeight: textFontWeight ?? FontWeight.w400),
          ),
          SizedBox(width: 10,),
          Icon(iconName ,size: 16,color: Colors.white,)
        ],
      ),
      style: ElevatedButton.styleFrom(
          side: borderColor != null
              ? BorderSide(
                  width: 1, color: borderColor ?? AppColors.mainOrangeColor)
              : null,
          backgroundColor: backgroundColor ?? AppColors.mainOrangeColor,
          shape: RoundedRectangleBorder(
              borderRadius: borderRadius ?? BorderRadius.circular(15)),
          fixedSize: fixedSize ?? Size(130.w, 50.h)),
    );
  }
}
