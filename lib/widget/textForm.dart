import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monitoring/widget/colors.dart';


class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    super.key,
    required this.hintText,
    this.controller,
    this.fillColor,
    this.hintTextColor,
    this.keyboardType,
    this.obscureText = false,
    this.validator,
    this.prefixIcon,
    this.prefixIconColor,
    this.suffixIcon,
    this.suffixIconColor,
    this.maxHeight,
    this.maxWidth,
    this.hintTextSize,
    this.maxLines,
    this.hintFontWeight,
    this.cursorColor,
    this.iconName
  });

  final String hintText;
  final TextEditingController? controller;
  final Color? fillColor;
  final Color? hintTextColor;
  final TextInputType? keyboardType;
  final bool obscureText;
  final String? Function(String?)? validator;
  final String? prefixIcon;
  final Color? prefixIconColor;
  final String? suffixIcon;
  final Color? suffixIconColor;
  final double? maxHeight;
  final double? maxWidth;
  final double? hintTextSize;
  final int? maxLines;
  final FontWeight? hintFontWeight;
  final Color? cursorColor;
  final Icon? iconName;
  @override
  Widget build(BuildContext context) {
    return
    TextFormField(
      maxLines: maxLines,
   
      // onChanged: onChanged,
      // controller: controller,
      validator: validator,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      obscureText: obscureText,
      textAlign: TextAlign.start,
      cursorColor: cursorColor ?? AppColors.mainWhiteColor,
      decoration: InputDecoration(
        // suffixIcon:iconName,
      
        errorStyle: TextStyle(
          color: AppColors.blueColor,
        ),
        suffixIconColor: suffixIconColor,
        contentPadding: EdgeInsetsDirectional.symmetric(
          horizontal: 10.w,vertical: 10.h
        ),
        constraints: BoxConstraints(
          maxWidth: maxWidth ?? 700.w,
          maxHeight: maxHeight ?? 0.07.sh,
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: AppColors.mainBlackColor,
          ),
          borderRadius: BorderRadius.circular(15.r),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
           borderSide: BorderSide(
            color: AppColors.blueColor,
            width:2
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.blueColor, width: 0.0013.sw),
          borderRadius: BorderRadius.circular(15.r),
        ),
        filled: true,
        hintStyle: TextStyle(
          color:  AppColors.mainBlackColor,
          fontSize: hintTextSize ?? 18.sp,
          fontWeight:  FontWeight.bold,
        ),
        hintText: hintText+":",

        fillColor: fillColor ?? AppColors.mainBlackColor,
      ),
      keyboardType: keyboardType ?? TextInputType.text,
    );
  }
}
