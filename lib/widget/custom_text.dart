import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum TextStyleType {
  TITLE, // 25px
  SUBTITLE, // 20px
  BODYBIG, // 20px
  BODY, // 17px
  SMALL, // 14px
  CUSTOM,
}

class CustomText extends StatelessWidget {
  const CustomText({
    super.key,
    required this.textType,
    required this.text,
    this.textColor,
    this.fontSize,
    this.fontWeight,
    this.textAlign = TextAlign.center,
    this.textDecoration = TextDecoration.none,
  });

  final TextStyleType textType;
  final String text;
  final Color? textColor;
  final TextAlign? textAlign;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextDecoration? textDecoration;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
      softWrap: true,
      maxLines: 6,
      style: getTextStyle(),
    );
  }

  TextStyle getTextStyle() {
    switch (textType) {
      case TextStyleType.TITLE:
        return TextStyle(
          height: 1.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: 25.sp,
          fontWeight: FontWeight.w900,
        );

      case TextStyleType.SUBTITLE:
        return TextStyle(
          height: 1.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: 22.sp,
          fontWeight: fontWeight ?? FontWeight.w700,
        );

      case TextStyleType.BODYBIG:
        return TextStyle(
          height: 1.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: 20.sp,
          fontWeight: fontWeight ?? FontWeight.w600,
        );

      case TextStyleType.BODY:
        return TextStyle(
          height: 1.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: 17.sp,
          fontWeight: fontWeight ?? FontWeight.w400,
        );

      case TextStyleType.SMALL:
        return TextStyle(
          height: 1.2.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: 14.sp,
          fontWeight: fontWeight ?? FontWeight.w200,
        );

      case TextStyleType.CUSTOM:
        return TextStyle(
          height: 1.h,
          decoration: textDecoration,
          color: textColor,
          fontSize: fontSize,
          fontWeight: fontWeight,
        );
    }
  }
}
