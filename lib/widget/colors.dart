import 'package:flutter/material.dart';

class AppColors {
  static Color mainOrangeColor = Color.fromRGBO(252, 96, 17, 1);
  static Color secondaryGreyColor = Color.fromRGBO(124, 125, 126, 1);
  static Color secondaryWhiteColor = Color.fromRGBO(255, 255, 255, 1);
  static Color Grey1 = Color.fromRGBO(47, 51, 60, 1); //used
  static Color Grey2 = Color.fromRGBO(43, 47, 56, 1); //used
  static Color secondaryBlackColor = Color.fromRGBO(74, 75, 77, 1);
  static Color blueColor = Color.fromARGB(255, 4, 77, 137); //used
  static Color redColor = Color.fromRGBO(221, 75, 57, 1);
  static Color brown=Color.fromARGB(255, 71, 60, 33);
  static Color greencolor = Color.fromRGBO(20, 223, 37, 1);
  static Color Grey = Color.fromRGBO(87, 86, 86, 1);
  static Color greycolor1 = Color.fromRGBO(139, 139, 139, 1);
  static Color mainBlackColor = Color.fromRGBO(31, 31, 39, 1); //used
  static Color mainWhiteColor = Color.fromRGBO(255, 255, 255, 1); //used
  static Color mainPurpleColor = Color.fromRGBO(110, 8, 150, 1);
}
