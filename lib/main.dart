import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:monitoring/login_view/login_view.dart';
import 'package:monitoring/notification/notification_admin.dart';
import 'package:monitoring/notification/notification_employee.dart';
import 'package:monitoring/notification/assign_notification.dart';
import 'package:monitoring/profile/profile_admin_view.dart';
import 'package:monitoring/statistics/statistics_admin.dart';
import 'package:monitoring/profile/profile_employee_view.dart';
import 'package:monitoring/widget/colors.dart';

void main() {
  initHive();
  runApp(const App());
}

initHive() async {
  await Hive.initFlutter();
  await Hive.openBox('showCase');
  Hive.box('showCase').put("skipped", false);
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent),
    );

    return ScreenUtilInit(
      child: GetMaterialApp(
        title: 'Introduction screen',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.blue),
        home: const OnBoardingPage(),
      ),
    );
  }
}

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  OnBoardingPageState createState() => OnBoardingPageState();
}

class OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Hive.box('showCase').put("skipped", true);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_) => LoginAndroidView()),
    );
  }

  Widget _buildFullscreenImage() {
    return Image.asset(
      'assets/12.jpg',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget _buildImage(String assetName, [double width = 400]) {
    return Image.asset(
      'assets/$assetName',
      width: width,
      height: 500,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);

    const pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      bodyPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Colors.white,
      allowImplicitScrolling: true,
      // autoScrollDuration: 3000,
      // infiniteAutoScroll: true,

      globalFooter: SizedBox(
        width: double.infinity,
        height: 60,
        child: ElevatedButton(
          child: const Text(
            'Let\'s go right away!',
            style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 4, 77, 137)),
          ),
          onPressed: () => _onIntroEnd(context),
        ),
      ),
      pages: [
        PageViewModel(
          title: "WELCOME",
          body:
              " We are glad to have you with us..            We kindly ask you to adhere to the instructions.",
          image: _buildImage('10077.jpg'),
          decoration: pageDecoration,
        ),
        //  PageViewModel(
        //     title: "Kids and teens",
        //     body:
        //         "Kids and teens can track their stocks 24/7 and place trades that you approve.",
        //     image: Container(margin: EdgeInsets.only(top:60 ),
        //     child: _buildImage('23.png')),
        //     decoration: pageDecoration,
        //   ),
      ],
      onDone: () => _onIntroEnd(context),
      onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      // showSkipButton: true,
      skipOrBackFlex: 0,
      nextFlex: 0,
      showBackButton: false,
      //rtl: true, // Display as right-to-left
      back:
          const Icon(Icons.arrow_back, color: Color.fromARGB(255, 17, 89, 148)),
      skip: const Text('Skip',
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Color.fromARGB(255, 17, 89, 148))),
      next: const Icon(Icons.arrow_forward,
          color: Color.fromARGB(255, 17, 89, 148)),
      done: const Text('Done',
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Color.fromARGB(255, 17, 89, 148))),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(12),
      controlsPadding: kIsWeb
          ? const EdgeInsets.all(45.0)
          : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Colors.white,
        activeColor: Colors.white,
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      showDoneButton: false,
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: LoginAndroidView(),
    );
  }
}
