// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;

// class User {
//   final String password;
//   final String email;

//   User({required this.password, required this.email});

//   factory User.fromJson(Map<String, dynamic> json) {
//     return User(
//       password: json['password'],
//       email: json['email'],
//     );
//   }
// }

// class UserListScreen extends StatefulWidget {
//   @override
//   _UserListScreenState createState() => _UserListScreenState();
// }

// class _UserListScreenState extends State<UserListScreen> {
//   List<User> users = [];

//   @override
//   void initState() {
//     super.initState();
//     fetchUsers();
//   }

//   Future<void> fetchUsers() async {
//     Uri url = Uri.parse('http://127.0.0.1:8000/ai/login/');
//     try {
//       final response = await http.get(url);
//       if (response.statusCode == 200) {
//         final List<dynamic> responseData = json.decode(response.body);
//         List<User> fetchedUsers =
//             responseData.map((userJson) => User.fromJson(userJson)).toList();
//         setState(() {
//           users = fetchedUsers;
//         });
//       } else {
//         print('Request failed with status: ${response.statusCode}');
//       }
//     } catch (error) {
//       print('Error: $error');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('User List'),
//       ),
//       body: ListView.builder(
//         itemCount: users.length,
//         itemBuilder: (ctx, index) {
//           return ListTile(
//             title: Text(users[index].name),
//             subtitle: Text(users[index].email),
//           );
//         },
//       ),
//     );
//   }
// }
