import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';
import 'package:monitoring/notification/assign_notification.dart';
import 'package:monitoring/widget/Custom_AppBar.dart';
import 'package:monitoring/widget/colors.dart';

// Security Alert: Unauthorized Individual
class NotificationListView extends StatelessWidget {
// const NotificationListView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: 'Notifications',
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildNotificationCard(context),
            SizedBox(height: 16.0),
            buildNotificationCard1(context),
            SizedBox(height: 16.0),
            buildNotificationCard2(context),
            SizedBox(height: 16.0),
            buildNotificationCard3(context),
            SizedBox(height: 16.0),
            buildNotificationCard4(context),
            SizedBox(height: 16.0),
            buildNotificationCard5(context),
          ],
        ),
      ),
    );
  }

  Widget buildNotificationCard(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color.fromARGB(255, 89, 112, 188),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (_) => ImageDialog(),
                );
              },
              child: Container(
                height: 60.h,
                width: 60.w,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/doctors-4.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Security:Unauthorized\n Individual',
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 8.0),
                Icon(
                  Icons.location_on,
                  color: AppColors.redColor,
                  size: 25,
                ),
                const SizedBox(width: 4.0),
                const Text('D1', style: TextStyle(fontSize: 18.0)),
              ],
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 8.0),
                    Icon(
                      Icons.calendar_month,
                      color: AppColors.blueColor,
                      size: 25,
                    ),
                    const SizedBox(width: 4.0),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        "at:2024:02:03 3:56",
                        // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                        style: const TextStyle(fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    Get.to(AssignNotification());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline,
                        color: AppColors.brown,
                        size: 22.sp,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        'Assign Task',
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: AppColors.blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildNotificationCard2(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
          side: const BorderSide(
            color: Color.fromARGB(255, 89, 112, 188),
          ),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ListView(
          shrinkWrap: true,
          children: [
            ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 60.h,
                  width: 60.w,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage('assets/profile3.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Security:Unauthorized\n Individual',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  const SizedBox(width: 8.0),
                  Icon(
                    Icons.location_on,
                    color: AppColors.redColor,
                    size: 25,
                  ),
                  const SizedBox(width: 4.0),
                  const Text('F51', style: TextStyle(fontSize: 18.0)),
                ],
              ),
              subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.calendar_month,
                        color: AppColors.blueColor,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 30),
                        child: Text(
                          " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: const TextStyle(fontSize: 17.0),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  InkWell(
                    onTap: () {
                      Get.to(AssignNotification());
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.add_circle_outline,
                          color: AppColors.brown,
                          size: 22.sp,
                        ),
                        SizedBox(width: 8.0),
                        Text(
                          'Assign Task',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: AppColors.blueColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildNotificationCard1(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color.fromARGB(255, 89, 112, 188),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (_) => ImageDialog(),
                );
              },
              child: Container(
                height: 60.h,
                width: 60.w,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/elias.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  ' Employee Elias mak\n left early 1h',
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 8.0),
                //   Icon(
                //     Icons.location_on,
                //     color: AppColors.redColor,
                //     size: 25,
                //   ),
                //   const SizedBox(width: 4.0),
                //   const Text('F13', style: TextStyle(fontSize: 18.0)),
              ],
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 8.0),
                    Icon(
                      Icons.calendar_month,
                      color: AppColors.blueColor,
                      size: 25,
                    ),
                    const SizedBox(width: 4.0),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                        style: const TextStyle(fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    Get.to(AssignNotification());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline,
                        color: AppColors.brown,
                        size: 22.sp,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        'Assign Task',
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: AppColors.blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildNotificationCard4(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color.fromARGB(255, 89, 112, 188),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (_) => ImageDialog(),
                );
              },
              child: Container(
                height: 60.h,
                width: 60.w,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/profile-icon-female1.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Employee Sara Adnan\n is 1h late',
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 8.0),
                // Icon(
                //   Icons.location_on,
                //   color: AppColors.redColor,
                //   size: 25,
                // ),
                // const SizedBox(width: 4.0),
                // const Text('F2C', style: TextStyle(fontSize: 18.0)),
              ],
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 8.0),
                    Icon(
                      Icons.calendar_month,
                      color: AppColors.blueColor,
                      size: 25,
                    ),
                    const SizedBox(width: 4.0),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        "at:2024:02:03 3:10",
                        // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                        style: const TextStyle(fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    Get.to(AssignNotification());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline,
                        color: AppColors.brown,
                        size: 22.sp,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        'Assign Task',
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: AppColors.blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildNotificationCard5(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color.fromARGB(255, 89, 112, 188),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (_) => ImageDialog(),
                );
              },
              child: Container(
                height: 60.h,
                width: 60.w,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/profile4.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Employee Ali AS is 1h late',
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 8.0),
                // Icon(
                //   Icons.location_on,
                //   color: AppColors.redColor,
                //   size: 25,
                // ),
                // const SizedBox(width: 4.0),
                // const Text('F3A', style: TextStyle(fontSize: 18.0)),
              ],
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 8.0),
                    Icon(
                      Icons.calendar_month,
                      color: AppColors.blueColor,
                      size: 25,
                    ),
                    const SizedBox(width: 4.0),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        "at:2024:02:03 2:20",
                        // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                        style: const TextStyle(fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    Get.to(AssignNotification());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline,
                        color: AppColors.brown,
                        size: 22.sp,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        'Assign Task',
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: AppColors.blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildNotificationCard3(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: const BorderSide(
          color: Color.fromARGB(255, 89, 112, 188),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (_) => ImageDialog(),
                );
              },
              child: Container(
                height: 60.h,
                width: 60.w,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/zs.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Security:Unauthorized\n Individual',
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 8.0),
                Icon(
                  Icons.location_on,
                  color: AppColors.redColor,
                  size: 25,
                ),
                const SizedBox(width: 4.0),
                const Text('F12', style: TextStyle(fontSize: 18.0)),
              ],
            ),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 8.0),
                    Icon(
                      Icons.calendar_month,
                      color: AppColors.blueColor,
                      size: 25,
                    ),
                    const SizedBox(width: 4.0),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        "at:2024:02:03 3:40",
                        // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                        style: const TextStyle(fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    Get.to(AssignNotification());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline,
                        color: AppColors.brown,
                        size: 22.sp,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        'Assign Task',
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: AppColors.blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: 400,
        height: 400,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: ExactAssetImage('assets/34.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
