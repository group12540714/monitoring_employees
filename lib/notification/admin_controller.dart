import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<void> getNotifications() async {
  // var url = Uri.parse('http://10.0.2.2:8000/ai/ainotifications/');
  // Replace with your backend server URL

  var response =
      await get(Uri.parse('http://10.0.2.2:8000/ai/ainotifications/'));
  print(response);
  if (response.statusCode == 200) {
    print('Data sent successfully');
  } else {
    print('Failed to send data. Error: ${response.statusCode}');
  }
}

// Usage:///
//sendUserData('example@example.com', 'password123');