import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monitoring/widget/Custom_AppBar.dart';
import 'package:monitoring/widget/colors.dart';

// class NotificationEmployee extends StatefulWidget {
//   const NotificationEmployee({Key? key}) : super(key: key);

//   @override
//   State<NotificationEmployee> createState() => _NotificationEmployeeState();
// }

// class _NotificationEmployeeState extends State<NotificationEmployee> {
//   List<bool> isDone = List.generate(8, (_) => false);
//   List<bool> isStarted = List.generate(8, (_) => false);
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class NotificationEmployee extends StatefulWidget {
  @override
  _NotificationEmployeeState createState() => _NotificationEmployeeState();
}

class _NotificationEmployeeState extends State<NotificationEmployee> {
  List<bool> isDone3 = List.generate(8, (_) => false);
  List<bool> isStarted3 = List.generate(8, (_) => false);
  List<bool> isDone = List.generate(8, (_) => false);
  List<bool> isStarted = List.generate(8, (_) => false);
  List<bool> isDone1 = List.generate(8, (_) => false);
  List<bool> isStarted1 = List.generate(8, (_) => false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildNotificationCard(context),
            SizedBox(height: 16.0),
            buildNotificationCard1(context),
            SizedBox(height: 16.0),
            buildNotificationCard2(context),
            SizedBox(height: 16.0),
            buildNotificationCard3(context),
            SizedBox(height: 16.0),
            buildNotificationCard4(context),
            SizedBox(height: 16.0),
            buildNotificationCard5(context),
          ],
        ),
      ),
    );
  }

  Widget buildNotificationCard(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/zs.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By:	administrationa ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Please go to help Customer',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                "F1D2",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:03 3:46",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone3[index],
                          onChanged: (value) {
                            setState(() {
                              isDone3[index] = value ?? false;
                              isStarted3[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted3[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted3[index] = value ?? false;
                              isDone3[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildNotificationCard1(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/profile3.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By: administration ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Please go to help Customer',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                " F5D1",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:03 2:56",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone1[index],
                          onChanged: (value) {
                            setState(() {
                              isDone1[index] = value ?? false;
                              isStarted1[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted1[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted1[index] = value ?? false;
                              isDone1[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildNotificationCard2(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/send.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By: administration ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'customer_increase\n go to work in',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                " f1D3",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:02 2:50",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone[index],
                          onChanged: (value) {
                            setState(() {
                              isDone[index] = value ?? false;
                              isStarted[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted[index] = value ?? false;
                              isDone[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildNotificationCard3(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/send.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By: administration ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'customer_increase\n go to work in',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                " F12 ",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:02 8:00",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone[index],
                          onChanged: (value) {
                            setState(() {
                              isDone[index] = value ?? false;
                              isStarted[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted[index] = value ?? false;
                              isDone[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildNotificationCard4(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/profile3.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By:	administrationa ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Please go to help Customer',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                "F5D1",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:03 3:28",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone[index],
                          onChanged: (value) {
                            setState(() {
                              isDone[index] = value ?? false;
                              isStarted[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted[index] = value ?? false;
                              isDone[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildNotificationCard5(BuildContext context) {
    return Container(
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        separatorBuilder: (context, index) => SizedBox(height: 16),
        itemBuilder: (context, index) {
          return Card(
            color: Color.fromARGB(97, 29, 70, 100).withOpacity(0.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(
                color: Color.fromARGB(255, 50, 100, 120),
                width: 1.7,
              ),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (_) => ImageDialog(),
                  );
                },
                child: Container(
                  height: 90.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 6.0,
                      image: AssetImage('assets/send.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Column(
                children: [
                  Text(
                    'Sender By:administration ',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Please go to help Customer',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.apartment,
                              color: Colors.blue,
                              size: 25,
                            ),
                            const SizedBox(width: 4.0),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                " F6",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      Icon(
                        Icons.punch_clock,
                        color: Colors.blue,
                        size: 25,
                      ),
                      const SizedBox(width: 4.0),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text(
                          "at:2024:02:03 3:56",
                          // " at: ${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${DateFormat('hh:mm').format(DateTime.now())}",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isDone[index],
                          onChanged: (value) {
                            setState(() {
                              isDone[index] = value ?? false;
                              isStarted[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Checkbox(
                          activeColor: Color.fromARGB(255, 47, 128, 50),
                          value: isStarted[index],
                          onChanged: (value) {
                            setState(() {
                              isStarted[index] = value ?? false;
                              isDone[index] = false;
                            });
                          },
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: 400,
        height: 400,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: ExactAssetImage('assets/send.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
