import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:monitoring/profile/profile_admin_view.dart';
import 'package:monitoring/profile/profile_employee_view.dart';
import 'package:monitoring/widget/Custom_AppBar.dart';
import 'package:monitoring/widget/colors.dart';
import 'package:monitoring/widget/custom_button.dart';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:monitoring/widget/divider.dart';
import 'package:monitoring/widget/textForm.dart';

class AssignNotification extends StatefulWidget {
  const AssignNotification({super.key});

  @override
  State<AssignNotification> createState() => _AssignNotificationState();
}

class _AssignNotificationState extends State<AssignNotification> {
  @override
  List<String> hints = ["Notification Type", "Employee Name"];
  List<bool> isOpen = [false, false, false];
  final List<DropdownMenuItem<String>> items1 = [
    ...[
      "security ",
      "customer_support ",
      "customer_increase",
      "Employee_left_early",
      "Employee late"
    ].map((item) => DropdownMenuItem<String>(
          value: item,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.tr,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp,
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              CustomVerticalDivider(
                width: 1.sw,
                height: 1.h,
              )
            ],
          ),
        )),
  ];

  final List<DropdownMenuItem<String>> items3 = [
    ...["1", "2", "3", "4"].map((item) => DropdownMenuItem<String>(
          value: item,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.tr,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp,
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              CustomVerticalDivider(width: 1.sw, height: 1.h)
            ],
          ),
        )),
  ];

  final List<DropdownMenuItem<String>> items2 = [
    ...[
      "Ali Ahmad",
      "Elie Assa",
      "Soliman yan",
      "Youshi Isma",
      "Ghazal mak",
      "Hadi aid",
      "Tom Jones"
    ].map((item) => DropdownMenuItem<String>(
          value: item,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.tr,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp,
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              CustomVerticalDivider(
                width: 1.sw,
                height: 1.h,
              )
            ],
          ),
        )),
  ];
  File? _image;

  Future<void> _selectImage() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBarCustom(title: "Assignment of a task"),
        body: ListView(
          primary: true,
          shrinkWrap: true,
          children: [
            Container(
              // color:AppColors.blueColor.withOpacity(0.3),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: _selectImage,
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey[300],
                      ),
                      child: _image != null
                          ? ClipOval(
                              child: Image.file(
                                _image!,
                                fit: BoxFit.cover,
                              ),
                            )
                          : Icon(
                              Icons.camera_alt,
                              size: 50,
                            ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0.h),
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 30.h,
                        ),
                        // (60.h).ph,
                        // CustomText(
                        //   textType: TextStyleType.CUSTOM,
                        //   fontWeight: FontWeight.w900,
                        //   fontSize: 100.sp,
                        //   text: 'Team Management',
                        //   textColor: AppColors.mainWhiteColor,
                        // ),

                        // SizedBox(height:200.h,),
                        //  (200.h).ph,
                        Column(
                          children: [
                            SizedBox(
                                child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              itemCount: 2,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 7.w, vertical: 4.h),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton2<String>(
                                        onMenuStateChange: (_) {
                                          setState(() {
                                            isOpen[index] = true;
                                          });
                                        },
                                        buttonStyleData: ButtonStyleData(
                                          // height: 55.h,
                                          width: 150.w,
                                          padding: const EdgeInsets.only(
                                              left: 14, right: 14),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(14),
                                            border: Border.all(
                                              color: AppColors.blueColor,
                                              width: 2,
                                            ),
                                            color: AppColors.mainWhiteColor,
                                          ),
                                          elevation: 2,
                                        ),
                                        dropdownStyleData:
                                            const DropdownStyleData(
                                          decoration:
                                              BoxDecoration(border: null),
                                          offset: Offset(0, -1),
                                        ),

                                        // value: controller.selectedType.value,
                                        hint: RichText(
                                          text: TextSpan(
                                            text: hints[index],
                                            style: TextStyle(
                                              // fontWeight: FontWeight.bold,
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.mainBlackColor,
                                            ),
                                            children: <TextSpan>[
                                              TextSpan(text: ""),
                                            ],
                                          ),
                                        ),

                                        // Text(
                                        //   // adminDashboardController.isEditedUser.value==true?
                                        //   // index==1?adminDashboardController.selectedRoleStr.value:
                                        //   // index==2?adminDashboardController.selectedDeptStr.value:index==3?
                                        //   // adminDashboardController.selectedBranchStr.value:

                                        //   textAlign: TextAlign.center,
                                        //   style:
                                        // ),
                                        items: index == 0 ? items1 : items2,

                                        iconStyleData: !isOpen[index]
                                            ? IconStyleData(
                                                icon: Icon(
                                                  Icons.arrow_drop_down_rounded,
                                                  color: AppColors.blueColor,
                                                  size: 30,
                                                ),
                                                iconSize: 50.r,
                                              )
                                            : IconStyleData(
                                                icon: Icon(
                                                  Icons.arrow_drop_up_rounded,
                                                  color: AppColors.Grey2,
                                                  size: 30,
                                                ),
                                                iconSize: 50.r,
                                              ),
                                        isExpanded: true,
                                        onChanged: (y) {
                                          setState(() {
                                            hints[index] = y!;
                                            isOpen[index] = false;
                                          });
                                        },
                                      ),
                                    ));
                              },
                            )),
                            SizedBox(
                              height: 3.h,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.h),
                              child: CustomTextFormField(
                                maxWidth: 1300.w,
                                maxLines: 8,
                                hintText: 'Location',
                                iconName: Icon(
                                  Icons.message,
                                  color: AppColors.blueColor,
                                ),
                                fillColor: AppColors.mainWhiteColor,
                                hintTextSize: 16.sp,
                                hintFontWeight: FontWeight.w400,
                                hintTextColor: AppColors.brown,
                                maxHeight: 50,
                              ),
                            ),
                            SizedBox(
                              height: 7.h,
                            ),
                            //  (20.h).ph,
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.h),
                              child: CustomTextFormField(
                                maxWidth: 1300.w,
                                maxLines: 8,
                                hintText: 'Message',
                                iconName: Icon(
                                  Icons.message,
                                  color: AppColors.blueColor,
                                ),
                                fillColor: AppColors.mainWhiteColor,
                                hintTextSize: 16.sp,
                                hintFontWeight: FontWeight.w400,
                                hintTextColor: AppColors.brown,
                                maxHeight: 90,
                              ),
                            ),
                            SizedBox(
                              height: 30.h,
                            ),
                            CustomButton(
                              text: 'Assign',
                              iconName: Icons.assignment,
                              onPressed: () {
                                Get.to(ProfileAdminView());
                                //  loginController.login();
                              },
                              backgroundColor: AppColors.blueColor,
                              textSize: 18.sp,
                              //  fixedSize: Size(350.w, 50.h),
                            )
                          ],
                        ),
                      ],
                    )),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
