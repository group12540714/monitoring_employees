const avatarList = [
  {
    'avatar': 'assets/profile1.jpg',
  },
  {
    'avatar': 'assets/profile2.jpg',
  },
  {
    'avatar': 'assets/profile4.jpg',
  },
  {
    'avatar': 'assets/profile-icon-female1.jpg',
  },
  {
    'avatar': 'assets/profile6.jpg',
  },
  {
    'avatar': 'assets/profile7.jpg',
  },
];

const userList = [
  {
    'avatar': 'assets/profile-icon-female1.jpg',
    'name': 'Sara Adnan',

    'message': 'Take a sick leave',
    //  'Ok!'
    'time': '15:34'
  },
  {
    'avatar': 'assets/profile1.jpg',
    'name': 'Justin O\'Moore',
    'message': 'I can\'t help customer in K21',
    'time': '12:50'
  },
  {
    'avatar': 'assets/profile2.jpg',
    'name': 'Youshaa Ismaeel',
    'message': 'Ok, Thanks!',
    'time': '12:35'
  },
  {
    'avatar': 'assets/profile4.jpg',
    'name': 'Ali Ahmad',
    'message': 'Hey, Iwant to take a vacation ',
    'time': '11:39'
  },
  {
    'avatar': 'assets/profile6.jpg',
    'name': 'Ghazal Mak',
    'message': 'Oh,Bye',
    'time': '08:41'
  },
  {
    'avatar': 'assets/profile7.jpg',
    'name': 'Elie Assa',
    'message': 'Hey there! whats up? Is everything fine ',
    'time': '08:41'
  },
];

const messages = [
  {
    'from': 'sender',
    'message': 'hi',
    'time': '11:16',
  },
  {
    'from': 'receiver',
    'message': 'hello',
    'time': '11:18',
  },
  {
    'from': 'sender',
    'message': 'whats up! where are you?',
    'time': '11:20',
  },
  {
    'from': 'receiver',
    'message': 'I am sick. I am at home',
    'time': '11:25',
  },
  {
    'from': 'sender',
    'message': 'Get Well soon.',
    'time': '11:30',
  },
  {
    'from': 'receiver',
    'message': 'Thank You',
    'time': '11:30',
  },
  {
    'from': 'sender',
    'message': 'Take a sick leave',
    'time': '14:48',
  },
  {
    'from': 'receiver',
    'message': 'Ok!',
    'time': '11:38',
  },
  {
    'from': 'sender',
    'message': 'Bye',
    'time': '11:38',
  },
  {
    'from': 'receiver',
    'message': 'Bye',
    'time': '11:39',
  },
];
